﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


    public List<AudioClip> audioClip;

    public enum ESFXList
    {
        SFX_AIRDIVER,
        SFX_BEAUTIFUL,
        SFX_EXIT,
        SFX_FELLTOSLOW,
        SFX_FOUR,
        SFX_FREEZE,
        SFX_ONE,
        SFX_PLAYER,
        SFX_PRINCESSDIE,
        SFX_PRINCESSGETREADY,
        SFX_PRINCESSLAUGH1,
        SFX_PRINCESSLAUGH2,
        SFX_SELECT,
        SFX_START,
        SFX_THREE,
        SFX_TWO,
        SFX_WINSTHEGAME,
        SFX_PLAYERONELOSE,
        SFX_PLAYERTWOLOSE,
        SFX_PLAYERTHREELOSE,
        SFX_PLAYERFOURLOSE,

        SFX_PLAYERONEWIN,
        SFX_PLAYERTWOWIN,
        SFX_PLAYERTHREEWIN,
        SFX_PLAYERFOURWIN,

        SFX_MAX
    }

    public ESFXList sfxList;



    private AudioClip qAudioClip;



    // Use this for initialization
    void Start () {
        //DontDestroyOnLoad(this.gameObject);	
	}

    public void SFXPlay(ESFXList _sfx)
    {
        AudioSource.PlayClipAtPoint(audioClip[(int)_sfx], Camera.main.transform.position);
    }


    public void SFXPlay(ESFXList _sfx, float _delay)
    {
        if (_delay <= 0f)
            AudioSource.PlayClipAtPoint(audioClip[(int)_sfx], Camera.main.transform.position);
        else
        {
            qAudioClip = audioClip[(int)_sfx];
            Invoke("SFXPlayQueued", _delay);
        }
    }


    private void SFXPlayQueued()
    {
        AudioSource.PlayClipAtPoint(qAudioClip, Camera.main.transform.position);
    }


    public float SFXGetLength(ESFXList _sfx)
    {
        return audioClip[(int)_sfx].length;
    }

    
    public void SFXPlayCustomPlayer(int playerID, bool bWin)
    {
        //SFXPlay(ESFXList.SFX_PLAYER);

        switch (playerID)
        {
            case 0:

                if (bWin)
                    SFXPlay(ESFXList.SFX_PLAYERONEWIN);
                else
                    SFXPlay(ESFXList.SFX_PLAYERONELOSE);

                break;

            case 1:


                if (bWin)
                    SFXPlay(ESFXList.SFX_PLAYERTWOWIN);
                else
                    SFXPlay(ESFXList.SFX_PLAYERTWOLOSE);

                break;

            case 2:

                if (bWin)
                    SFXPlay(ESFXList.SFX_PLAYERTHREEWIN);
                else
                    SFXPlay(ESFXList.SFX_PLAYERTHREELOSE);


                break;

            case 3:
        
                if (bWin)
                    SFXPlay(ESFXList.SFX_PLAYERFOURWIN);
                else
                    SFXPlay(ESFXList.SFX_PLAYERFOURLOSE);

                break;
        }


    }




}
