﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Invoke("PlayWinSound", 5f);
        Invoke("GoBackToMenu", 10f);
	}
	
	// Update is called once per frame

    void PlayWinSound()
    {
        GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_WINSTHEGAME);
    }



    void GoBackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
