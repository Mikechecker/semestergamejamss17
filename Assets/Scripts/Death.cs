﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour {

    void OnTriggerEnter2D (Collider2D other)
    {
        //Debug.Log(GetComponent<GameData>().players.Count);
        if (other.CompareTag("Player"))
        {
            GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_PRINCESSDIE);


            if (other.name == "Player Lila")
            {
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_PLAYERONELOSE);
                other.gameObject.GetComponent<PlayerData>().isAlive = false;
            }

            if (other.name == "Player Green")
            {
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_PLAYERTWOLOSE);
                other.gameObject.GetComponent<PlayerData>().isAlive = false;
            }

            if (other.name == "Player Blue")
            {
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_PLAYERTHREELOSE);
                other.gameObject.GetComponent<PlayerData>().isAlive = false;
            }

            if (other.name == "Player Yellow")
            {
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_PLAYERFOURLOSE);
                other.gameObject.GetComponent<PlayerData>().isAlive = false;
            }

         
            //GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_FELLTOSLOW, 1f);
            GameData.instance.players.Remove(other.gameObject.GetComponent<PlayerData>());
            //Debug.Log("meh");
            Destroy(other.gameObject);
            
            if (GameData.instance.players.Count == 1)
            {
                Debug.Log("Player " + GameData.instance.GetWinPlayerID() + " wins the game");

                GameObject.Find(GameData.instance.GetWinPlayerID().ToString()).transform.GetChild(0).gameObject.SetActive(true);

                Destroy(GameData.instance.players[0].gameObject);
            }


            }
        }
    }
