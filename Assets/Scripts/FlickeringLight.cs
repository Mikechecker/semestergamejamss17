﻿using UnityEngine;
using System.Collections;

public class FlickeringLight : MonoBehaviour
{
	
	// Your light gameObject here.
	public Light[] lt;

	// Array of random values for the intensity.
	private float[] smoothing;

	public bool enableSmoothing = true;
	public float intensityInSmoothingMode = 3;
	public int smoothingLength = 100;


	public bool disableGameObject = false;
	public GameObject[] go;

	void Start ()
	{
		//light = this.GetComponent<Light> ();

		 smoothing = new float[smoothingLength];

		// Initialize the array.
		for (int i = 0; i < smoothing.Length; i++) {
			smoothing [i] = .0f;
		}
	}
	
	void Update ()
	{


		if (enableSmoothing) {
			float sum = .0f;
		
			// Shift values in the table so that the new one is at the
			// end and the older one is deleted.
			for (int i = 1; i < smoothing.Length; i++) {
				smoothing [i - 1] = smoothing [i];
				sum += smoothing [i - 1];
			}
		
			// Add the new value at the end of the array.
			smoothing [smoothing.Length - 1] = Random.value;
			sum += smoothing [smoothing.Length - 1];
		
			// Compute the average of the array and assign it to the
			// light intensity.

			foreach (Light l in lt) {
				l.intensity = (sum / smoothing.Length) * intensityInSmoothingMode;
			}
		} else {
			if (Random.value > 0.9) { //a random chance

				if (!disableGameObject)
				{
					foreach (Light l in lt)
					{
						if (l.enabled == true)
						{ //if the light is on...
							l.enabled = false; //turn it off
						}
						else
						{
							l.enabled = true; //turn it on
						}
					}
				} else
				{
					foreach (GameObject g in go)
					{
						if (g.activeSelf == true)
						{ //if the light is on...
							g.SetActive(false); //turn it off
						}
						else
						{
							g.SetActive(true); //turn it on
						}
					}
				}
			}
		}
	}

}