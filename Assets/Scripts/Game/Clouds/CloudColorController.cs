﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudColorController : MonoBehaviour
{
    public PlayerData.PlayerColor color;
    private Spine.Unity.SkeletonAnimator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Spine.Unity.SkeletonAnimator>();
        //List<int> colors = new List<int>();
        //foreach (GameObject obj in GameData.instance.cloudSpawner.objects)
        //{
        //    if (obj.tag == "Cloud")
        //    {
        //        int col = (int)obj.GetComponent<CloudColorController>().color;
        //        if (!colors.Contains(col) && col != (int)PlayerData.PlayerColor.none)
        //        {
        //            colors.Add(col);
        //        }
        //    }
        //}

        //color = (PlayerData.PlayerColor)Util.RandomInt(0, 3, colors);
        //if ((int)color > 3)
            color = (PlayerData.PlayerColor)Util.RandomInt(0, 3);
        animator.skeleton.SetSkin(color.ToString());
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void FreezeCloud()
    {
        animator.skeleton.skin = null;
        animator.skeleton.SetSkin("Frozen");

    }
}
