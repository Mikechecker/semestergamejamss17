﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour
{


    public bool freeze = true;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!freeze)
        {
            transform.Translate(new Vector3(0, GlobalSettings.instance.speed, 0) * Time.deltaTime);
            if (transform.position.y >= GlobalSettings.instance.despawnHeight)
            {
                Destroy(gameObject);
            }
        }

    }
}
