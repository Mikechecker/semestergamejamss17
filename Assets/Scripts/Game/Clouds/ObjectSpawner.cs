﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{

    public GameObject[] objectPrefab;
    public Vector3 objectSpawnAreaPoint1;
    public Vector3 objectSpawnAreaPoint2;
    public Vector2 spawnTimeVariationDelay;
    public float spawnTimeDelay;
    public int maxObjects;
    public float spawnDistance;

    internal List<GameObject> objects;
    private float respawnDelay;
    // Use this for initialization
    void Start()
    {
        objects = new List<GameObject>();
        respawnDelay = 0;
    }

    // Update is called once per frame
    void Update()
    {
        objects.Remove(null);
        if (spawnTimeDelay == 0)
            return;
        if (respawnDelay <= 0 &&
            (maxObjects == 0 || objects.Count < maxObjects))
        {
            List<Vector3> exceptions = new List<Vector3>();
            foreach (GameObject obj in objects) 
            {
                if (obj != null)
                    exceptions.Add(obj.transform.position);
            }

            Vector3 spawnPoint = Util.RandomVector3(objectSpawnAreaPoint1, objectSpawnAreaPoint2, spawnDistance, exceptions);

            if(true)//if (Util.RandomInt(0, 1) == 0)
            {
                objects.Add(Instantiate<GameObject>(objectPrefab[0], spawnPoint, Quaternion.identity));
                respawnDelay = spawnTimeDelay + Util.RandomNumber(spawnTimeVariationDelay.x, spawnTimeVariationDelay.y);
            }
            else
            {
                Instantiate<GameObject>(objectPrefab[1], spawnPoint, Quaternion.identity);
                respawnDelay = spawnTimeDelay + Util.RandomNumber(spawnTimeVariationDelay.x, spawnTimeVariationDelay.y);
            }

        }
        respawnDelay -= Time.deltaTime;
    }

}
