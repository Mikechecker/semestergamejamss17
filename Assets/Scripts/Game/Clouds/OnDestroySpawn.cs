﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroySpawn : MonoBehaviour {
    public GameObject obj;
	
        void OnDisable()
    {

        Instantiate(obj, transform.position, Quaternion.identity);
    }
}
