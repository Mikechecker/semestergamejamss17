﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRotator : MonoBehaviour
{

    bool noTarget = true;
    Quaternion qTo;
    //public float speed = 1.25f;
    public float rotateSpeed = 3.0f;
    float timer = 0.0f;
    public float maxAngle;
    public float interval = 1f;


    void Start()
    {
        qTo = Quaternion.Euler(new Vector3(Random.Range(-0, 0), 0.0f, Random.Range(-180.0f, 180.0f)));

    }

    void Update()
    {

        timer += Time.deltaTime;

        if (noTarget == true)
        {//when not targeting hero     
            if (timer > interval)
            { // timer resets at 2, allowing .5 s to do the rotating
                qTo = Quaternion.Euler(new Vector3(Random.Range(-0, 0), 0.0f, Random.Range(-maxAngle, maxAngle)));
                timer = 0.0f;
            }
            transform.localRotation = Quaternion.Slerp(transform.rotation, qTo, Time.deltaTime * rotateSpeed);
            //transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

    }
}









