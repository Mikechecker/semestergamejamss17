﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownCollisionHandler : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Platform" || other.tag == "Cloud")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }

    }
}
