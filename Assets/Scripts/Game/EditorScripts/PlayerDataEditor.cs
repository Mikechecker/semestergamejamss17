﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(PlayerData))]
public class PlayerDataEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("SetColor"))
        {
            PlayerData myScript = (PlayerData)target;
            myScript.GetComponent<SpriteRenderer>().sprite = GlobalSettings.instance.colorSprites[(int)myScript.color];
        }

    }
}
