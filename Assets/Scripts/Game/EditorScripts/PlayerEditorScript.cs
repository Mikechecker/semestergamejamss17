﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerMovement))]
public class PlayerEditorScript : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
       

        if (GUILayout.Button("Kick Left"))
        {
            PlayerMovement myScript = (PlayerMovement)target;
            myScript.playerData.playerBehaviour.GetKicked(Vector2.right);
        }
        if (GUILayout.Button("Kick Right"))
        {
            PlayerMovement myScript = (PlayerMovement)target;
            myScript.playerData.playerBehaviour.GetKicked(Vector2.left);
        }
        GUILayout.EndHorizontal();
    }
}

