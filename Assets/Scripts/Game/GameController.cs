﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [System.Serializable]
    public class Phase
    {
        public string name;
        public Vector2 averageCloudSpawnTimeDelay;
        public float duration;
        public SoundManager.ESFXList sound;
    }

    public Phase[] phases;

    private Phase actualPhase;
    private int phaseCounter = -1;
    private float actualPhaseDuration;
    // Use this for initialization
    void Start()
    {
        nextPhase();
    }

    // Update is called once per frame
    void Update()
    {
        if (actualPhase.duration == 0)
            return;


        GameData.instance.cloudSpawner.spawnTimeDelay = Util.Slerp(
            actualPhase.averageCloudSpawnTimeDelay.x,
            actualPhase.averageCloudSpawnTimeDelay.y,
            1 - actualPhaseDuration / actualPhase.duration);

        actualPhaseDuration -= Time.deltaTime;
        if (actualPhaseDuration < 0)
        {
            nextPhase();
        }
    }

    private void nextPhase()
    {
       
        phaseCounter++;
        actualPhase = phases[phaseCounter]; actualPhaseDuration = actualPhase.duration;
        GameData.instance.cloudSpawner.spawnTimeDelay = actualPhase.averageCloudSpawnTimeDelay.x;
        GlobalSettings.instance.soundManager.SFXPlay(actualPhase.sound);

        print("Next Phase: " + actualPhase.name);
    }
}
