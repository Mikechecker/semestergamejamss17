﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {

    public static GameData instance;
    public float losingHeight;
    public float leftBorder;
    public float rightBorder;
    public float playerWidth;

    public List<PlayerData> players;
    internal ObjectSpawner cloudSpawner;
	// Use this for initialization
	void Awake() {
        instance = this;
        cloudSpawner = GetComponent<ObjectSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public List<PlayerData> GetPlayersInSameLayer(PlayerData player)
    {
        List<PlayerData> results = new List<PlayerData>();
        foreach(PlayerData pl in players)
        {
            if (pl.color != player.color && pl.lives == player.lives)
                results.Add(pl);
        }
        return results;
    }


    public int GetWinPlayerID()
    {
        foreach (PlayerData pl in players)
        {
            if (pl.isPlayerAlive())
                return (int)pl.color;
        }

        return -1;
    }
}
