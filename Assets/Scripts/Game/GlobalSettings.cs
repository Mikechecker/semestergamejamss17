﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettings : MonoBehaviour
{
    public static GlobalSettings instance;

    public Vector3 destroyAreaPointLeftUpper;
    public Vector3 destroyAreaPointRightLower;
    public Vector3 playingAreaPointLeftUpper;
    public Vector3 playingAreaPointRightLower;



    [Header("Clouds")]
    public float speed;
    public float despawnHeight;

    [Header("Player Settings")]
    public Sprite[] colorSprites;
    public int defaultLives;
    public float unclutchDistance;
    public float unclutchMaxSpeed;
    public float movementSpeed;

    public float startLevel;
    public float levelDistances;
    public float liveLostInvicibility;

    [Header("Ability Settings")]
    public GameObject kickLeft;
    public GameObject kickRight;
    public GameObject pushWave;
    public Sprite[] powerUpSprites;
    public int neededCloudsForPowerUp;

    [Header("Kick Settings")]
    public float kickSpeed;
    public float kickRange;
    public float kickStunTime;
    public float kickMoveTime;
    public float kickCooldown;

    [Header("PushWave Settings")]
    public float pushWaveSpeed;
    public float pushWaveRange;
    public float pushWaveMoveTime;

    [Header("Crown Settings")]
    public float crownSpeed;
    public GameObject crownPrefab;
    public float crownSpawnDistance;


    internal SoundManager soundManager;
    //[Header("Misc Abnility Settings")]
    //public float colorizerDuration;

    //public float skullDuration;

    void Awake()
    {
        instance = this; 
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }
}
