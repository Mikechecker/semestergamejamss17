﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour {
    public Vector3 direction;
    public float speed;
    public Vector3 livingAreaPoint1;
    public Vector3 livingAreaPoint2;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(direction.normalized * speed * Time.deltaTime);

        //if (Util.InBetween(transform.position, livingAreaPoint1, livingAreaPoint2))
        //    Destroy(gameObject);
	}
}
