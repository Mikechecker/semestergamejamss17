﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixPositions : MonoBehaviour {

    public bool fixX;
    public bool fixY;
    public bool fixZ;

    private Vector3 start;
	void Start()
    {
        start = transform.position;
    }
	
	void Update () {
        if (fixX)
            transform.position = new Vector3(start.x, transform.position.y, transform.position.z);
        if (fixY)
            transform.position = new Vector3(transform.position.x, start.y, transform.position.z);
        if (fixZ)
            transform.position = new Vector3(transform.position.x, transform.position.y, start.z);
    }
}
