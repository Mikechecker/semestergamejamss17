﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAbilities : MonoBehaviour
{
    bool hadPowerUp = false;
    private Animator animator;

    [SerializeField]
    internal bool readyKick = true;
    internal PlayerData playerData;
    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        playerData = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        //check if ready for powerUp
        if (readyKick && Input.GetButtonDown("B_" + ((int)playerData.color + 1)))
            Kick();

        if (Input.GetButtonDown("A_" + ((int)playerData.color + 1)))
        {
            bool result = false;
            switch (playerData.powerUp)
            {
                case PlayerData.PowerUp.none:
                    break;

                case PlayerData.PowerUp.crown:
                    result = Crowns();
                    break;

                case PlayerData.PowerUp.pushWave:
                    result = PushWave();
                    break;

                //case PlayerData.PowerUp.skull:
                //    result = Skull();
                //    break;
            }
            if (result)
                playerData.powerUp = PlayerData.PowerUp.none;
        }

    }

    private bool Crowns()
    {
        Vector3 dir = new Vector3(0,
          -(Input.GetAxis("L_YAxis_" + ((int)playerData.color + 1))),
            0).normalized;
        if (dir.magnitude == 0)
            return false;

        GameObject obj =
            Instantiate<GameObject>(
                GlobalSettings.instance.crownPrefab,
                transform.position + dir * GlobalSettings.instance.crownSpawnDistance,
                Quaternion.identity);
        ObjectMovement crown = obj.GetComponent<ObjectMovement>();
        crown.direction = dir;
        crown.speed = GlobalSettings.instance.crownSpeed;

        crown.livingAreaPoint1 = GlobalSettings.instance.destroyAreaPointLeftUpper;
        crown.livingAreaPoint2 = GlobalSettings.instance.destroyAreaPointRightLower;
        return true;
    }

    private bool Skull()
    {
        GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_FREEZE);
        foreach (GameObject obj in GameData.instance.cloudSpawner.objects)
        {
            CloudColorController cloud = obj.GetComponent<CloudColorController>();
            cloud.FreezeCloud();
        }
        return true;
    }

    private bool PushWave()
    {
        print("push");
        //get PLayers
        foreach (PlayerData player in GameData.instance.players)
        {
            if ((player.gameObject.transform.position - transform.position).magnitude < GlobalSettings.instance.pushWaveRange)
            {
                player.playerBehaviour.GetPushed(
                    new Vector3(
                        player.gameObject.transform.position.x - transform.position.x, 0, 0).normalized);
            }
        }
        Instantiate(GlobalSettings.instance.pushWave, transform.position, Quaternion.identity);
        return true;
    }
    private bool Kick()
    {
        //get PLayers

        PlayerData closestPlayer = null;
        float closestDistance = 1000;
        List<PlayerData> pls = GameData.instance.GetPlayersInSameLayer(playerData);
        foreach (PlayerData player in pls)
        {
            float distance = Mathf.Abs((player.gameObject.transform.position - transform.position).magnitude);
            if (distance < GlobalSettings.instance.kickRange && distance <= closestDistance)
            {
                closestDistance = distance;
                closestPlayer = player;

            }
        }
        if (closestPlayer != null)
        {
            closestPlayer.playerBehaviour.GetKicked(
           new Vector3(
               closestPlayer.gameObject.transform.position.x - transform.position.x, 0, 0).normalized);

            if (transform.position.x > closestPlayer.gameObject.transform.position.x)
            {
                Instantiate(GlobalSettings.instance.kickLeft, transform.position, GlobalSettings.instance.kickLeft.transform.rotation);
                animator.SetBool("leftKick", true);
            }
            else
            {
                Instantiate(GlobalSettings.instance.kickRight, new Vector3(transform.position.x, transform.position.y, 0), GlobalSettings.instance.kickRight.transform.rotation);
                animator.SetBool("rightKick", true);
            }

            Invoke("ResetKickAnimations", 0.2f);
            Invoke("ReadyKick", GlobalSettings.instance.kickCooldown);
            readyKick = false;
            return true;
        }
        return false;
    }

    internal void CheckForPowerUps()
    {
        if (playerData.collectedClouds >= GlobalSettings.instance.neededCloudsForPowerUp)
        {
            if (hadPowerUp && playerData.lives < 3)
            {
                playerData.lives++;
                hadPowerUp = false;
            }
            else
                hadPowerUp = true;
            playerData.collectedClouds = 0;
            GetRandomAbility();
        }
    }

    internal void GetRandomAbility()
    {
        playerData.powerUp = (PlayerData.PowerUp)Util.RandomInt(1, 2);
    }

    private void ReadyKick()
    {
        readyKick = true;
    }

    private void ResetKickAnimations()
    {
        animator.SetBool("leftKick", false);
        animator.SetBool("rightKick", false);
    }

}
