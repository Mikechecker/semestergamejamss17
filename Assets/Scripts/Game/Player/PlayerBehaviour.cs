﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public struct Kick
    {
        public float duration;
        public Vector2 direction;
        public float speed;
    }

    internal Kick actualKick;
    internal PlayerData playerData;
    // Use this for initialization
    void Start()
    {
        playerData = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetKicked(Vector2 direction)
    {
        actualKick = new Kick();
        actualKick.duration = GlobalSettings.instance.kickMoveTime;
        actualKick.direction = direction;
        actualKick.speed = GlobalSettings.instance.kickSpeed;
        playerData.isKickedStunned = true;
        Invoke("UnStun", GlobalSettings.instance.kickStunTime);
    }

    public void GetPushed(Vector2 direction)
    {
        actualKick = new Kick();
        actualKick.duration = GlobalSettings.instance.pushWaveMoveTime;
        actualKick.direction = direction;
        actualKick.speed = GlobalSettings.instance.pushWaveSpeed;
        playerData.isKickedStunned = true;
        Invoke("UnStun", GlobalSettings.instance.pushWaveMoveTime);
    }

    private void UnStun()
    {
        playerData.isKickedStunned = false;
    }
}
