﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionHandler : MonoBehaviour
{
    private Animator animator;
    internal PlayerData playerData;
    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        playerData = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!playerData.isLiveLostInvincible)
        {
            if (other.tag == "Platform")
            {
                playerData.lives -= 1;
                animator.SetTrigger("hit");
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_SELECT);
                playerData.isLiveLostInvincible = true;
                Invoke("UnInvincibility", GlobalSettings.instance.liveLostInvicibility);
                Destroy(other.gameObject);
            }
            if (other.tag == "Cloud")
            {
                if (other.GetComponent<CloudColorController>().color != playerData.color)
                {
                    playerData.lives -= 1;
                    animator.SetTrigger("hit");
                    GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_SELECT);
                    playerData.isLiveLostInvincible = true;
                    Invoke("UnInvincibility", GlobalSettings.instance.liveLostInvicibility);
                    other.GetComponent<Animator>().SetBool("Collect",true);
                    Destroy(other.gameObject.GetComponent<Collider2D>());
                }
                else
                {
                    playerData.collectedClouds++;
                    playerData.playerAbilities.CheckForPowerUps();
                    other.GetComponent<Animator>().SetBool("Collect", true);
                    Destroy(other.gameObject.GetComponent<Collider2D>());
                }

               
            }
            else if (other.tag == "Crown")
            {
                playerData.lives -= 1;
                animator.SetTrigger("hit");
                GlobalSettings.instance.soundManager.SFXPlay(SoundManager.ESFXList.SFX_SELECT);
                playerData.isLiveLostInvincible = true;
                Invoke("UnInvincibility", GlobalSettings.instance.liveLostInvicibility);
                Destroy(other.gameObject);
            }
        }
    }

    private void UnInvincibility()
    {
        playerData.isLiveLostInvincible = false;
    }
}
