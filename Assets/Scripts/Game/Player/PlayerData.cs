﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public enum PlayerColor
    {
        Magenta = 0,
        Green = 1,
        blue = 2,
        Yellow = 3,
        none = 4
    }
    public enum PowerUp
    {
        none = 0,
        pushWave = 1, 
        crown = 2

    }

    public int lives;

    public bool isAlive = true;

    public PlayerColor color;
    [SerializeField]
    internal bool isKickedStunned;
    [SerializeField]
    internal bool isLiveLostInvincible;


    [SerializeField]
    public PowerUp powerUp;
    internal int collectedClouds;

    internal PlayerMovement playerMovement;
    internal PlayerBehaviour playerBehaviour;
    internal PlayerLiveBehaviour playerLiveBehaviour;
    internal PlayerCollisionHandler playerCollisionHandler;
    internal PlayerAbilities playerAbilities;
    void Start()
    {
        lives = GlobalSettings.instance.defaultLives;
        playerMovement = GetComponent<PlayerMovement>();
        playerBehaviour = GetComponent<PlayerBehaviour>();
        playerLiveBehaviour = GetComponent<PlayerLiveBehaviour>();
        playerCollisionHandler = GetComponent<PlayerCollisionHandler>();
        playerAbilities = GetComponent<PlayerAbilities>();
       // GetComponent<SpriteRenderer>().sprite = GlobalSettings.instance.colorSprites[(int)color];
    }

    internal bool isStunned()
    {
        return isKickedStunned || isLiveLostInvincible;
    }

    internal bool isPlayerAlive()
    {
        return isAlive;
    }
}
