﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLiveBehaviour : MonoBehaviour
{
   
    private int currentLiveLevel;
    private float oldPosition;
    private float newPosition;
    private float liveLostInvincibilityDuration;
    internal PlayerData playerData;
    // Use this for initialization
    void Start()
    {
       
        playerData = GetComponent<PlayerData>();
        currentLiveLevel = playerData.lives;
        newPosition = GlobalSettings.instance.startLevel - playerData.lives * GlobalSettings.instance.levelDistances;
        transform.position = new Vector3(transform.position.x, GlobalSettings.instance.startLevel - playerData.lives * GlobalSettings.instance.levelDistances, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentLiveLevel != playerData.lives)
        {
            liveLostInvincibilityDuration = GlobalSettings.instance.liveLostInvicibility;
            currentLiveLevel = playerData.lives;
            oldPosition = transform.position.y;
            newPosition = GlobalSettings.instance.startLevel - playerData.lives * GlobalSettings.instance.levelDistances;
        }

        if (liveLostInvincibilityDuration > 0)
        {
            //change lane
            float height = Mathf.Lerp(newPosition, oldPosition, liveLostInvincibilityDuration / GlobalSettings.instance.liveLostInvicibility);
            transform.position = new Vector3(transform.position.x, height, 0);
            liveLostInvincibilityDuration -= Time.deltaTime;
        }
        else
        {
            transform.position = new Vector3(transform.position.x, newPosition, 0);
        }
    }
}
