﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    internal PlayerData playerData;
    // Use this for initialization
    void Start()
    {
        playerData = GetComponent<PlayerData>();

    }

    // Update is called once per frame
    void Update()
    {
        PlayerControl();
        PassiveMovement();
    }

    private void PassiveMovement()
    {
        if (playerData.playerBehaviour.actualKick.duration > 0)
        {
            float speed = Mathf.Lerp(0, playerData.playerBehaviour.actualKick.speed, playerData.playerBehaviour.actualKick.duration / GlobalSettings.instance.kickStunTime);
            Move(new Vector3(playerData.playerBehaviour.actualKick.direction.x, playerData.playerBehaviour.actualKick.direction.y, 0) * speed * Time.deltaTime);

            playerData.playerBehaviour.actualKick.duration -= Time.deltaTime;
        }

        List<PlayerData> players = GameData.instance.GetPlayersInSameLayer(playerData);
        foreach (PlayerData player in players)
        {
            if ((player.gameObject.transform.position - transform.position).magnitude < GlobalSettings.instance.unclutchDistance)
            {

                Move(new Vector3(
                           player.gameObject.transform.position.x - transform.position.x, 0, 0).normalized
                           * -1 * GlobalSettings.instance.unclutchMaxSpeed * Time.deltaTime);
            }
        }

        //BorderChecks
        if (transform.position.x < GameData.instance.leftBorder + GameData.instance.playerWidth / 2)
            transform.position = new Vector3(GameData.instance.leftBorder + GameData.instance.playerWidth / 2, transform.position.y, 0);

        if (transform.position.x > GameData.instance.rightBorder - GameData.instance.playerWidth / 2)
            transform.position = new Vector3(GameData.instance.rightBorder - GameData.instance.playerWidth / 2, transform.position.y, 0);
    }

    private void PlayerControl()
    {
        if (!playerData.isStunned())
        {
           Move(new Vector3(Input.GetAxis("L_XAxis_" + ((int)playerData.color + 1)) * GlobalSettings.instance.movementSpeed * Time.deltaTime, 0, 0));
        }
    }

    private void Move(Vector3 direction)
    {
        transform.position = transform.position + direction;
    }
}
