﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour {

    //public int spriteNumber;

    public float speed = 1;
    public int startpunkt;
    public int endpunkt;
    //public SpriteRenderer[] spriteList;

    public Sprite[] sprites;

    [System.Serializable]
    public class structSpriteData
    {
        public Color spriteColor = Color.white;
       // public float startPunkt;
       // public float endPunkt;
       public SpriteRenderer spriteRenderer;
    }

    public structSpriteData[] spriteData;
    private int number;

    public void Start() 
    {
        for (int i = 0; i < spriteData.Length; i++)
        {
            spriteData[i].spriteRenderer.color = spriteData[i].spriteColor;
        }
    }

    private void moveBackground()
    {
        for (int i = 0; i < spriteData.Length; i++)
        {
            spriteData[i].spriteRenderer.transform.Translate(Vector3.up * speed * Time.fixedDeltaTime);
            //if (spriteData[i].spriteRenderer.transform.position.y > spriteData[i].endPunkt)
            if (spriteData[i].spriteRenderer.transform.position.y >= endpunkt)
            {
                number = Random.Range (0, sprites.Length);
                Vector3 oldPosition = spriteData[i].spriteRenderer.transform.position;
                //spriteData[i].spriteRenderer.transform.position = new Vector3(oldPosition.x, spriteData[i].startPunkt , oldPosition.z);
                spriteData[i].spriteRenderer.transform.position = new Vector3(oldPosition.x, startpunkt + (spriteData[i].spriteRenderer.transform.position.y - endpunkt), oldPosition.z);
                spriteData[i].spriteRenderer.sprite = sprites[number];
            } 
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        moveBackground();
        //for (int i = 0; i < spriteList.Length; i++)
        {
           // Debug.Log(transform.position);
        }
	}
}
