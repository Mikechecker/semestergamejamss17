﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlerpTest : MonoBehaviour {
    public float start;
    public float end;
    public float time;
    private float duration;
	// Use this for initialization
	void Start () {
        duration = time;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(duration, Util.Slerp(start, end, duration/time), 0);
        duration -= Time.deltaTime;
	}
}
