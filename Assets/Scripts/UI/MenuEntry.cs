﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuEntry : MonoBehaviour {

    public enum EFadeDir
    {
        DIR_UP,
        DIR_LEFT,
        DIR_RIGHT,
        DIR_DOWN
    }


    public enum EFadeState
    {
        FS_IN,
        FS_STAY,
        FS_OUT
    }

    private Transform thisTransform;

    private Vector3 targetPosIn;
    private Vector3 targetPosOut;


    [Header("Visible on Menu Page")]
    [SerializeField]
    private EPageType pageType = EPageType.PT_MENU_MAIN;


    [Header("Fade-in direction")]
    [SerializeField]
    private EFadeDir fadeDirIn = EFadeDir.DIR_DOWN;


    [Header("Fade-out direction")]
    [SerializeField]
    private EFadeDir fadeDirOut = EFadeDir.DIR_UP;


    [Header("Fading speeds")]
    [SerializeField]
    private float fadeDistanceHorizontal = 12f;

    [SerializeField]
    private float fadeDistanceVertical = 6f;


    [Header("Fade delay")]
    [SerializeField]
    private float fadeDelay = 0f;
    private float fadeDelayTimer = 0f;

    [Header("Selection")]
    [SerializeField]
    private MenuEntry selectNext;

    [SerializeField]
    private MenuEntry selectPrev;

    [SerializeField]
    private bool isSelected = false;

    [SerializeField]
    private float blinkSpeed = 10f;
    private SpriteRenderer thisSpriteRenderer;
    Color[] LerpTab = new Color[2];
    int counter = 0;


    [Header("Execution")]
    [SerializeField]
    public EPageType executePageType = EPageType.PT_MENU_MAIN;


    [Header("DEBUG")]
    [SerializeField]
    private EFadeState fadeState = EFadeState.FS_IN;
    

    // Use this for initialization
    void Awake () {
        thisTransform = this.transform;
        thisSpriteRenderer = thisTransform.GetComponent<SpriteRenderer>();

        LerpTab[0] = Color.white;
        LerpTab[1] = Color.gray;

        thisSpriteRenderer.color = Color.white;





        switch (fadeDirIn)
        {
            case EFadeDir.DIR_LEFT:
                targetPosIn = thisTransform.position + (Vector3.left * fadeDistanceHorizontal);
                break;

            case EFadeDir.DIR_RIGHT:
                targetPosIn = thisTransform.position + (Vector3.right * fadeDistanceHorizontal);
                break;

            case EFadeDir.DIR_UP:
                targetPosIn = thisTransform.position + (Vector3.up * fadeDistanceVertical);
                break;

            case EFadeDir.DIR_DOWN:
                targetPosIn = thisTransform.position + (Vector3.down * fadeDistanceVertical);
                break;
        }



        switch (fadeDirOut)
        {
            case EFadeDir.DIR_LEFT:
                targetPosOut = thisTransform.position + (Vector3.left * fadeDistanceHorizontal);
                break;

            case EFadeDir.DIR_RIGHT:
                targetPosOut = thisTransform.position + (Vector3.right * fadeDistanceHorizontal);
                break;

            case EFadeDir.DIR_UP:
                targetPosOut = thisTransform.position + (Vector3.up * fadeDistanceVertical);
                break;

            case EFadeDir.DIR_DOWN:
                targetPosOut = thisTransform.position + (Vector3.down * fadeDistanceVertical);
                break;
        }




    }
	
	// Update is called once per frame
	void Update () {
		
        switch(fadeState)
        {
            case EFadeState.FS_IN:

                fadeDelayTimer += Time.deltaTime;

                if(fadeDelayTimer >= fadeDelay)
                    this.transform.position = Vector3.Lerp(thisTransform.position, targetPosIn, Time.deltaTime);

                break;

            case EFadeState.FS_OUT:

                fadeDelayTimer += Time.deltaTime;
                if (fadeDelayTimer >= fadeDelay)
                    this.transform.position = Vector3.Lerp(thisTransform.position, targetPosOut, Time.deltaTime);

                break;

            case EFadeState.FS_STAY:

                break;
        }


        if(IsSelected())
        {
            float t = Time.deltaTime * blinkSpeed;

            thisSpriteRenderer.color = Color.Lerp(thisSpriteRenderer.color, LerpTab[counter], t);

            if (thisSpriteRenderer.color == LerpTab[counter])
            {
                counter++;
                counter = counter % LerpTab.Length;
            }
        }
        else
        {
            thisSpriteRenderer.color = LerpTab[0];
        }
	}



    public void SetFadeState(EPageType _pageType)
    {
        if(/*(_pageType != pageType) && */fadeState == EFadeState.FS_OUT)
        {
            fadeState = EFadeState.FS_IN;
            fadeDelayTimer = 0f;
        }

        else if(/*_pageType == pageType && */fadeState == EFadeState.FS_IN)
        {
            Debug.Log("MEP");
            fadeState = EFadeState.FS_OUT;
            fadeDelayTimer = 0f;            
        }
    }

    public void SelectNext()
    {
        if (IsSelected())
        {
            //Debug.Log("SelectNext");
            if (selectNext)
            {
                //Debug.Log(selectNext.name);
                selectNext.isSelected = true;
                isSelected = false;
            }
            else
            {
                //Debug.LogWarning("SelectNext not available");
            }       
        }
    }


    public void SelectPrev()
    {
        if (IsSelected())
        {
            //Debug.Log("SelectPrev");
            if (selectPrev)
            {
                //Debug.Log(selectPrev.name);
                selectPrev.isSelected = true;
                isSelected = false;
            }
            else
            {
                //Debug.LogWarning("SelectPrev not available");
            }    
        }
    }


    public bool IsSelected()
    {
        return isSelected;
    }


}
