﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum EPageType
{
    PT_MENU_START,
    PT_MENU_MAIN,
    PT_MENU_CREDITS,
    PT_MENU_QUIT,
    PT_LOAD_SCENE
}



public class MenuManager : MonoBehaviour {


    public enum EPageState
    {
        PS_IN,
        PS_OUT
    }

    private SoundManager soundManager;


    [Header("Menu Page State")]
    [SerializeField]
    private EPageState pageState;


    [Header("Menu Page Type")]
    [SerializeField]
    private EPageType pageType;

    
    [Header("Panel Item")]
    [SerializeField]
    private List<MenuEntry> panelItems;


    [Header("Input Move Delay")]

    [SerializeField]
    private float inputMoveTimer;

    private float inputButtonTimer;

    [SerializeField]
    private float inputMoveDelay = 1f;

    [SerializeField]
    private float inputButtonDelay = 1f;


	// Use this for initialization
	void Start () {
        //DontDestroyOnLoad(this.gameObject);

        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        MenuEntry[] panelItemsArr = GameObject.FindObjectsOfType<MenuEntry>();

        foreach (MenuEntry MI in panelItemsArr)
            panelItems.Add(MI);

        SetPageTypeTo(EPageType.PT_MENU_MAIN);
    }


    public void SetPageTypeTo(EPageType _pageType)
    {
        Debug.Log("MenuManager: LastPageType " + pageType.ToString());
        Debug.Log("MenuManager: SetPageTypeTo " + _pageType.ToString());

        pageType = _pageType;

        foreach (MenuEntry MI in panelItems)
            MI.SetFadeState(pageType);

        
    }


    public EPageType GetPageType()
    {
        return pageType;
    }


    private void SetState(EPageState _pageState)
    {
        Debug.Log("MenuManager: SetStateTo " + _pageState.ToString());
        pageState = _pageState;
    }


    private EPageState GetState()
    {
        return pageState;
    }

    private void SelectNext()
    {
        soundManager.SFXPlay(SoundManager.ESFXList.SFX_SELECT);
        foreach (MenuEntry MI in panelItems)
            MI.SelectNext();
    }

    private void SelectPrev()
    {
        soundManager.SFXPlay(SoundManager.ESFXList.SFX_SELECT);
        foreach (MenuEntry MI in panelItems)
            MI.SelectPrev();
    }

    private void Execute()
    {
        foreach(MenuEntry MI in panelItems)
        {
            if(MI.IsSelected())
            {
                Debug.Log("EXECUTE: " + MI.executePageType.ToString());

                switch(MI.executePageType)
                {
                    case EPageType.PT_MENU_START:
                        soundManager.SFXPlay(SoundManager.ESFXList.SFX_BEAUTIFUL);
                        LoadSceneDelayed(2);
                        break;

                    case EPageType.PT_MENU_QUIT:
                        soundManager.SFXPlay(SoundManager.ESFXList.SFX_EXIT);
                        break;
                }


                SetPageTypeTo(MI.executePageType);
            }
        }
    }



    private void ExitGameDelayed(float _delay)
    {
        Invoke("ExitGame", _delay);
    }

    private void ExitGame()
    {
        Application.Quit();
    }


    private void LoadSceneDelayed(float _delay)
    {
        Invoke("LoadScene", _delay);
    }


    private void LoadScene()
    {
        SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    void Update ()
    {
        inputMoveTimer += Time.deltaTime;
        inputButtonTimer += Time.deltaTime;

        if (Input.GetAxis("L_XAxis_1") > 0.3f || Input.GetAxis("DPad_XAxis_1") > 0.3f)
            if (inputMoveTimer >= inputMoveDelay)
            {
                inputMoveTimer = 0f;
                SelectNext();
            }

        if (Input.GetAxis("L_XAxis_1") < -0.3f || Input.GetAxis("DPad_XAxis_1") < -0.3f)
            if (inputMoveTimer >= inputMoveDelay)
            {
                inputMoveTimer = 0f;
                SelectPrev();
            }

        if(Input.GetButtonDown("A_1"))
        {
            Execute();
        }

	}
}
