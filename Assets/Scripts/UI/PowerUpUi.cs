﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpUi : MonoBehaviour
{

    [SerializeField]
    internal PlayerData player;
    [SerializeField]
    internal PlayerData.PowerUp shownPowerUp;

    // Use this for initialization
    void Start()
    {
        if (shownPowerUp == PlayerData.PowerUp.none)
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SpriteRenderer>().sprite = GlobalSettings.instance.powerUpSprites[(int)shownPowerUp - 1];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player.powerUp != shownPowerUp)
        {
            shownPowerUp = player.powerUp;
            if (shownPowerUp == PlayerData.PowerUp.none)
            {
                GetComponent<SpriteRenderer>().enabled = false;
            }
            else
            {
                GetComponent<SpriteRenderer>().enabled = true;

                if(((int)shownPowerUp - 1) < GlobalSettings.instance.powerUpSprites.Length)
                    GetComponent<SpriteRenderer>().sprite = GlobalSettings.instance.powerUpSprites[(int)shownPowerUp - 1];
            }
        }
    }
}
