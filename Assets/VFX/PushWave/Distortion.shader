// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|alpha-571-OUT,refract-9971-OUT;n:type:ShaderForge.SFN_Tex2d,id:1804,x:31747,y:33178,ptovrint:False,ptlb:RefractionTexture,ptin:_RefractionTexture,varname:node_1804,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_ValueProperty,id:2709,x:32049,y:33450,ptovrint:False,ptlb:RefractionValue,ptin:_RefractionValue,varname:node_2709,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_Vector1,id:571,x:32517,y:32965,varname:node_571,prsc:2,v1:0;n:type:ShaderForge.SFN_Lerp,id:8043,x:32256,y:33155,varname:node_8043,prsc:2|A-4968-OUT,B-1096-OUT,T-2365-OUT;n:type:ShaderForge.SFN_Tex2d,id:1829,x:31885,y:33368,ptovrint:False,ptlb:RefractionTextureMask,ptin:_RefractionTextureMask,varname:_RefractionTexture_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2365,x:32123,y:33299,varname:node_2365,prsc:2|A-1829-R,B-2709-OUT;n:type:ShaderForge.SFN_Append,id:1096,x:31957,y:33214,varname:node_1096,prsc:2|A-1804-R,B-1804-G;n:type:ShaderForge.SFN_Vector2,id:4968,x:32057,y:33134,varname:node_4968,prsc:2,v1:0,v2:0;n:type:ShaderForge.SFN_VertexColor,id:5695,x:31874,y:32848,varname:node_5695,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9971,x:32493,y:33155,varname:node_9971,prsc:2|A-8043-OUT,B-5695-A;proporder:1804-2709-1829;pass:END;sub:END;*/

Shader "Shader Forge/Distortion" {
    Properties {
        _RefractionTexture ("RefractionTexture", 2D) = "bump" {}
        _RefractionValue ("RefractionValue", Float ) = 1.5
        _RefractionTextureMask ("RefractionTextureMask", 2D) = "black" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _RefractionTexture; uniform float4 _RefractionTexture_ST;
            uniform float _RefractionValue;
            uniform sampler2D _RefractionTextureMask; uniform float4 _RefractionTextureMask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 _RefractionTexture_var = UnpackNormal(tex2D(_RefractionTexture,TRANSFORM_TEX(i.uv0, _RefractionTexture)));
                float4 _RefractionTextureMask_var = tex2D(_RefractionTextureMask,TRANSFORM_TEX(i.uv0, _RefractionTextureMask));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (lerp(float2(0,0),float2(_RefractionTexture_var.r,_RefractionTexture_var.g),(_RefractionTextureMask_var.r*_RefractionValue))*i.vertexColor.a);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
                float3 finalColor = 0;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,0.0),1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
